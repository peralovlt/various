import httpLoader from "./lib/vue-loader.js";

new Vue({
  el: "#app",
  components: {
    Appot: httpLoader("./src/App.vue")
  }
});
