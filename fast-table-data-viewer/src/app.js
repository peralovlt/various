define(function(require) {
  const Vue = require("Vue");

  new Vue({
    el: "#app",
    components: {
      Appot: VueLoader("./App.vue")
    },
    data: {
      fields: []
    }
  });
});
